This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Resource Registry Schema Client

## [v4.3.0-SNAPSHOT]

- Added support for model knowledge [#25922]


## [v4.2.1]

- Migrated code to reorganized E/R format [#24992]


## [v4.2.0]

- Enhanced gcube-bom version
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)
- Added the possibility for a client to add additional HTTP headers
- Added the possibility to create a client instance by specifying context


## [v4.1.0]

- Restrict the interface to accept ERElement classes and not all the Element classes [#21973]
- Client gets service URL using resource-registry-api lib utility [#23658]


## [v4.0.0] [r5.0.0]

- Switched JSON management to gcube-jackson [#19116]


## [v3.0.0] [r4.21.0] - 2020-03-30

- Refactored code to support IS Model reorganization (e.g naming, packages)
- Using gxREST in place of custom class of resource-registry-api [#11455]
- Refactored code to support renaming of Embedded class to Property [#13274]


## [v2.0.0] [r4.13.0] - 2018-11-20

- Using new resource-registry REST interface [#11904]


## [v1.0.0] [r4.10.0] - 2018-02-15

- First Release [#10248]

