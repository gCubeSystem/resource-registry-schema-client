package org.gcube.informationsystem.resourceregistry.schema;

import java.util.List;

import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.tree.Node;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceRegistrySchemaClientTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(ResourceRegistrySchemaClientTest.class);

	protected ResourceRegistrySchemaClient resourceRegistrySchemaClient;
	
	public ResourceRegistrySchemaClientTest() {
		if(ContextTest.RESOURCE_REGISTRY_URL !=null  && !ContextTest.RESOURCE_REGISTRY_URL.isEmpty()) {
			resourceRegistrySchemaClient = new ResourceRegistrySchemaClientImpl(ContextTest.RESOURCE_REGISTRY_URL);
		}else {
			resourceRegistrySchemaClient = ResourceRegistrySchemaClientFactory.create();
		}
	}
	
	
	@Test
	public void testRead() throws Exception {
		List<Type> types = resourceRegistrySchemaClient.read(Facet.class, true);
		Assert.assertTrue(types.size()>1);
		for(Type td : types) {
			logger.debug("{}", td);
		}
		
		types = resourceRegistrySchemaClient.read(Facet.class, false);
		Assert.assertTrue(types.size()==1);
		Type gotFacetDefinition = types.get(0);
		Type facetDefinition = TypeMapper.createTypeDefinition(Facet.class);
		
		
		Assert.assertTrue(gotFacetDefinition.getName().compareTo(facetDefinition.getName())==0);
		Assert.assertTrue(gotFacetDefinition.getDescription().compareTo(facetDefinition.getDescription())==0);
		
		Node<Type> node = resourceRegistrySchemaClient.getTypeTreeNode(Resource.class);
		logger.debug("{}",node);
		
		node = resourceRegistrySchemaClient.getTypeTreeNode(Facet.class);
		logger.debug("{}",node);
		
		node = resourceRegistrySchemaClient.getTypeTreeNode(Property.class);
		logger.debug("{}",node);
		
		node = resourceRegistrySchemaClient.getTypeTreeNode(IsRelatedTo.class);
		logger.debug("{}",node);

		node = resourceRegistrySchemaClient.getTypeTreeNode(ConsistsOf.class);
		logger.debug("{}",node);
	}
	
}
