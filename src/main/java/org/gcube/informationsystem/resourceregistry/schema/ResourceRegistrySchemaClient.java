package org.gcube.informationsystem.resourceregistry.schema;

import java.util.List;

import org.gcube.informationsystem.model.knowledge.ModelKnowledge;
import org.gcube.informationsystem.model.reference.ModelElement;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaNotFoundException;
import org.gcube.informationsystem.tree.Node;
import org.gcube.informationsystem.types.knowledge.TypeInformation;
import org.gcube.informationsystem.types.reference.Type;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface ResourceRegistrySchemaClient {

	public ModelKnowledge<Type, TypeInformation> getModelKnowledge();
	
	public void renewModelKnowledge();
	
	public void addHeader(String name, String value);
	
	public String create(String typeDefinitition) throws SchemaException, ResourceRegistryException;
	
	public <ME extends ModelElement> Type create(Class<ME> clz) throws SchemaException, ResourceRegistryException;

	public boolean exist(String typeName) throws ResourceRegistryException;

	public <ME extends ModelElement> boolean exist(Class<ME> clz) throws ResourceRegistryException;

	public String read(String typeName, Boolean polymorphic) throws SchemaNotFoundException, ResourceRegistryException;
	
	public <ME extends ModelElement> List<Type> read(Class<ME> clz, Boolean polymorphic)
			throws SchemaNotFoundException, ResourceRegistryException;

	public String read(String typeName, int level)
			throws SchemaNotFoundException, ResourceRegistryException;
	
	public <ME extends ModelElement> List<Type> read(Class<ME> clz, int level)
			throws SchemaNotFoundException, ResourceRegistryException;
	
	public Node<Type> getTypeTreeNode(String typeName)
			throws SchemaNotFoundException, ResourceRegistryException;
	
	public <ME extends ModelElement> Node<Type> getTypeTreeNode(Class<ME> clz)
			throws SchemaNotFoundException, ResourceRegistryException;

}
