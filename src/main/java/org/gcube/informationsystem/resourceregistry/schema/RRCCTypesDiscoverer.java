package org.gcube.informationsystem.resourceregistry.schema;

import java.util.Collection;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.model.knowledge.TypesDiscoverer;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.types.reference.Type;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class RRCCTypesDiscoverer implements TypesDiscoverer<Type> {

	protected ResourceRegistrySchemaClientImpl rrsc;
	
	public RRCCTypesDiscoverer(ResourceRegistrySchemaClientImpl rrsc) {
		this.rrsc = rrsc;
	}
	
	@Override
	public Collection<Type> discover(AccessType accessType) {
		try {
			return rrsc.getTypeFromServer(accessType.getTypeClass(), true);
		} catch (ResourceRegistryException e) {
			throw new RuntimeException(e);
		}
	}

}
